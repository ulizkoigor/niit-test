/*
 ============================================================================
 Name        : lab 1.5.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program prints string at center
 ============================================================================
 */

#include <stdio.h>
#include <string.h>

int main()
{
   char str [256];

   setlinebuf(stdout);

   printf("Enter string:\n");
   fgets(str,256,stdin);
   size_t len = strlen(str);
   int rightBorder = 40+len/2;
   char format[256];
   sprintf(format,"%c%ds",'%', rightBorder);
   printf(format, str);

   return 0;
}
