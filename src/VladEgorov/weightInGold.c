//
//  main.c
//  weightinGold
//
//  Created by Vladii Egorov on 23/09/16.
//  Copyright (c) 2016 Vladii Egorov. All rights reserved.
//

#include <stdio.h>

int main()
{
    int weight = 0;
    
    puts("Please enter your weight");
    scanf("%d",&weight);
    if(weight <= 0){
        puts("Not valid input.Try agian");
        scanf("%d",&weight);
    }
    printf("Your worth in gold %drub\n",weight*1850000);
    return 0;
}

