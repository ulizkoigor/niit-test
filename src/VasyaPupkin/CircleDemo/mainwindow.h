#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbRadius_clicked();
    void on_pbFerence_clicked();
    void on_pbSquare_clicked();

private:
    Ui::MainWindow *ui;
    double radius,ference,square;
};

#endif // MAINWINDOW_H
